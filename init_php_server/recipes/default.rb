# #ユーザ作成
# %w{webadmin}.each do |username|
#     user username do
#         supports :manage_home => true
#         home "/home/#{username}"
#         shell "/bin/bash"
#         password "webadmin5234"
#     end
# end

# # グループ作成
# group "apache" do
#   action:modify
#   group_name 'apache'
#   members "webadmin"
#   append  true
# end

# #Webルートディレクトリの所有者変更
# bash "chmod directory" do
#     user "root"
#     group "root"
#     cwd "/"
#     code "chown -R webadmin:apache /var/www/html"
# end

# bash "chmod mode" do
#   user "webadmin"
#   group  "apache"
#   cwd "/"
#   code "chmod 0775 -R /var/www/html"
# end

# PHP追加インストール
%w{php-mysql mysql-server}.each do |pkg|
  package pkg do
    action :install
  end
end

# デプロイユーザ
username="webni"

directory "/home/#{username}/.ssh" do
  owner username
  group  "opsworks"
  recursive true
  mode 0755
  action :create
#  not_if { File.exists "/home/#{username}/.ssh" }
end

# デプロイ鍵 public
cookbook_file "/home/#{username}/.ssh/id_rsa.pub" do
  owner  username
  group  "opsworks"
  mode   00600
  source "id_rsa.pub"
end

# デプロイ鍵 private
cookbook_file "/home/#{username}/.ssh/id_rsa" do
  owner  username
  group  "opsworks"
  mode   00600
  source "id_rsa"
end

# Appacheの設定の上書き
cookbook_file "/etc/httpd/conf/httpd.conf" do
  owner  "root"
  group  "root"
  mode   "0644"
  source "httpd.conf"
end

cookbook_file "/etc/httpd/conf.d/virtualhost.conf" do
  owner  "root"
  group  "root"
  mode   "0644"
  source "virtualhost.conf"
end

cookbook_file "/etc/httpd/conf.d/php.conf" do
  owner  "root"
  group  "root"
  mode   "0644"
  source "php.conf"
end

# Apache再起動
service "httpd" do
  action :restart
end

